package model.api;

import java.util.HashMap;

/**
 * @author Georgia Grigoriadou
 */

public interface Node {

    int getId();

    String getName();

    HashMap<Node, Float> getNeighbours();

    void addNeighbour(Node neighbour, float weight);

}
