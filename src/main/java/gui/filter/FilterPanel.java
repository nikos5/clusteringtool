package gui.filter;

import controller.visualization.Appearance;
import controller.visualization.GephiInitializer;
import gui.components.CustomTable;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Gewrgia
 */

abstract class FilterPanel extends JPanel {
    JPanel      panel;
    JScrollPane scrollPane ;

    FilterPanel(){
        initComponents();
    }

    private void initComponents(){
        scrollPane = new JScrollPane();
        panel      = new JPanel();

        panel.setBackground(Color.WHITE);
        scrollPane.setBorder(null);

        setLayout(new BorderLayout());

        panel.setLayout(new VerticalLayout());
        scrollPane.setViewportView(panel);
        add(scrollPane, BorderLayout.CENTER);
    }

    abstract void init();

    abstract boolean createFilters();

    abstract void rename(String oldID, String newID);

    abstract void reset();

    @SuppressWarnings("unchecked")
    ArrayList<String> getClusters() {
        ArrayList<String> list = new ArrayList(Appearance.partition.getSortedValues());
        if(list.contains(GephiInitializer.INDEPENDENT)){
            list.remove(GephiInitializer.INDEPENDENT);
        }
        Collections.sort(list);
        return list;
    }

    class ClusterPanel extends JPanel {
        private JPanel            titlePanel;
        private JPanel            tablePanel;
        private JLabel            titleLabel;
        private JCheckBox         checkBox;
        private JTable            table;
        private DefaultTableModel tableModel;

        ClusterPanel(String title) {
            titlePanel = new JPanel();
            tablePanel = new JPanel();
            titleLabel = new JLabel(title);
            checkBox   = new JCheckBox();

            table      = new CustomTable() {
                @Override
                public boolean isCellEditable(int row, int col) {
                    return col == 0;
                }
            };
            tableModel = (DefaultTableModel) table.getModel();
            table.setAutoCreateRowSorter(true);
            for (String cluster : getClusters()) {
                Object[] row = new Object[2];
                row[0]       = true;
                row[1]       = cluster;

                tableModel.addRow(row);
            }
            table.setModel(tableModel);
            table.getColumnModel().getColumn(0).setMinWidth(20);
            table.getColumnModel().getColumn(0).setMaxWidth(20);
            table.getColumnModel().getColumn(0).setWidth(20);
            table.setShowGrid(false);
            table.setIntercellSpacing(new Dimension(5, 0));
            checkBox.setSelected(true);
            checkBox.addActionListener(e -> {
                table.setVisible(checkBox.isSelected());
                for (int i = 0; i < tableModel.getRowCount(); i++) {

                    tableModel.setValueAt(checkBox.isSelected(), i, 0);

                }
            });

            titlePanel.setBackground(Color.WHITE);
            titlePanel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    table.setVisible(!table.isVisible());
                }
            });

            table.setVisible(false);

            JPanel space = new JPanel();
            space.setPreferredSize(new Dimension(20, table.getHeight()));

            table.setBackground(new Color(239, 239, 239));
            table.setRowSelectionAllowed(false);
            table.setFocusable(false);


            setLayout(new BorderLayout());
            titlePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
            tablePanel.setLayout(new BorderLayout());

            titlePanel.add(checkBox);
            titlePanel.add(titleLabel);

            tablePanel.add(space, BorderLayout.WEST);
            tablePanel.add(table, BorderLayout.CENTER);

            add(titlePanel, BorderLayout.NORTH);
            add(tablePanel, BorderLayout.CENTER);
        }

        ArrayList<String> getSelectedClusters() {
            ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                Boolean checked = (Boolean) tableModel.getValueAt(i, 0);
                if (checked) {
                    list.add((String) tableModel.getValueAt(i, 1));
                }
            }
            return list;
        }

        public void rename(String oldID, String newID) {
            for (int i = 0; i < tableModel.getRowCount(); i++) {
                String oldValue = (String) tableModel.getValueAt(i, 1);
                if (oldValue.equals(oldID)) {
                    tableModel.setValueAt(newID, i, 1);
                }
            }
        }
    }

}
