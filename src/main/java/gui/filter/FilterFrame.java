package gui.filter;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import controller.visualization.Filter;

import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.synth.SynthInternalFrameUI;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

public class FilterFrame extends JInternalFrame {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Filter");

    private JPanel        panel;
    private JPanel        buttonPanel;
    private JPanel        footerPanel;
    private FilterPanel   nodePanel;
    private FilterPanel   edgePanel;
    private JTabbedPane   tabbedPane;
    private JToolBar      toolBar;
    private JToggleButton nodeButton;
    private JToggleButton edgeButton;
    private JButton       filterButton;
    private JButton       resetButton;

    public FilterFrame() {
        setTitle(bundle.getString("Filter.name"));
        setFrameIcon(new ImageIcon(bundle.getString("Filter.filterIconPath")));
        initComponents();

        setContentPane(panel);
        setBorder(new BasicBorders.MarginBorder());
        setClosable(true);
        setVisible(true);

        SynthInternalFrameUI ui = (SynthInternalFrameUI) getUI();
        ui.propertyChange(new PropertyChangeEvent(this, JInternalFrame.IS_SELECTED_PROPERTY, 0, 0));
    }

    private void initComponents() {
        panel        = new JPanel();
        buttonPanel  = new JPanel();
        footerPanel  = new JPanel();
        nodePanel    = new FilterNodesPanel();
        edgePanel    = new FilterEdgesPanel();
        tabbedPane   = new JTabbedPane();
        toolBar      = new JToolBar();
        nodeButton   = new JToggleButton();
        edgeButton   = new JToggleButton();
        filterButton = new JButton();
        resetButton  = new JButton();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(nodeButton);
        buttonGroup.add(edgeButton);

        nodeButton.setText(bundle.getString("Filter.node"));
        edgeButton.setText(bundle.getString("Filter.edge"));

        nodeButton.setMargin(new Insets(0,0,0,0));
        edgeButton.setMargin(new Insets(0,0,0,0));

        filterButton.setText(bundle.getString("Filter.name"));
        filterButton.setIcon(new ImageIcon(bundle.getString("Filter.playIconPath")));

        resetButton.setIcon(new ImageIcon(bundle.getString("Filter.resetIconPath")));
        resetButton.setToolTipText((bundle.getString("Filter.reset")));

        Dimension d = new Dimension(40, 22);
        nodeButton.setMinimumSize(d);
        edgeButton.setMinimumSize(d);
        nodeButton.setMaximumSize(d);
        edgeButton.setMaximumSize(d);
        nodeButton.setPreferredSize(d);
        edgeButton.setPreferredSize(d);
        nodeButton.setSelected(true);
        toolBar.setBorder(null);

        resetButton.setFocusPainted(false);
        resetButton.setMargin(new Insets(0, 0, 0, 0));
        resetButton.setContentAreaFilled(false);
        resetButton.setBorderPainted(false);
        resetButton.setOpaque(false);

        toolBar.setFloatable(false);

        addComponents();
        addListeners();

        UIManager.put("TabbedPane.tabInsets", new Insets(-10, -10, -10, -10));
        tabbedPane.setUI(new WindowsTabbedPaneUI());
    }

    private void addComponents() {
        setLayout(new BorderLayout());
        panel.setLayout(new BorderLayout());
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        footerPanel.setLayout(new BorderLayout());

        buttonPanel.add(nodeButton);
        buttonPanel.add(edgeButton);

        footerPanel.add(resetButton, BorderLayout.WEST);
        footerPanel.add(filterButton, BorderLayout.EAST);

        toolBar.add(buttonPanel);

        tabbedPane.add(nodePanel, 0);
        tabbedPane.add(edgePanel, 1);

        panel.add(tabbedPane, BorderLayout.CENTER);
        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(footerPanel, BorderLayout.SOUTH);
    }

    private void addListeners(){
        nodeButton.addActionListener(e -> tabbedPane.setSelectedIndex(0));
        edgeButton.addActionListener(e -> tabbedPane.setSelectedIndex(1));
        filterButton.addActionListener(
                e -> {
                    Filter.reset();
                    for (Component c : tabbedPane.getComponents()) {
                        if (FilterPanel.class.isAssignableFrom(c.getClass())) {
                            if (!((FilterPanel) c).createFilters()) return;
                        }
                    }
                    Filter.filter();
                });
        resetButton.addActionListener(e -> {
            for (Component c : tabbedPane.getComponents()) {
                if (FilterPanel.class.isAssignableFrom(c.getClass())) {
                    ((FilterPanel) c).reset();
                }
            }
            Filter.reset();
        });
    }

    public void initFilters() {
        tabbedPane.removeAll();
        nodePanel.init();
        edgePanel.init();
        tabbedPane.add(nodePanel, 0);
        tabbedPane.add(edgePanel, 1);
    }

    public void reset() {
        tabbedPane.removeAll();
        nodePanel = new FilterNodesPanel();
        edgePanel = new FilterEdgesPanel();
        tabbedPane.add(nodePanel, 0);
        tabbedPane.add(edgePanel, 1);
        nodeButton.setSelected(true);
    }

    public void rename(String oldID, String newID){
        for (Component c : tabbedPane.getComponents()) {
            if (FilterPanel.class.isAssignableFrom(c.getClass())) {
                ((FilterPanel) c).rename(oldID, newID);
            }
        }
    }
    public void setVisible(boolean b) {
        boolean visible = isVisible();
        super.setVisible(b);
        firePropertyChange("visible", visible, b);
    }

}
