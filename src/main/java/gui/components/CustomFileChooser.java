package gui.components;


import gui.resources.FileProperties;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

public class CustomFileChooser extends JFileChooser {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Components");

    private FileNameExtensionFilter gephiExtFilter   = new FileNameExtensionFilter(gephiFilter,   gephi);
    private FileNameExtensionFilter gdfExtFilter     = new FileNameExtensionFilter(gdfFilter,     gdf);
    private FileNameExtensionFilter vnaExtFilter     = new FileNameExtensionFilter(vnaFilter,     vna);
    private FileNameExtensionFilter gefxExtFilter    = new FileNameExtensionFilter(gefxFilter,    gefx);
    private FileNameExtensionFilter graphMLExtFilter = new FileNameExtensionFilter(graphMLFilter, graphML);
    private FileNameExtensionFilter gmlExtFilter     = new FileNameExtensionFilter(gmlFilter,     gml);
    private FileNameExtensionFilter pdfExtFilter     = new FileNameExtensionFilter(pdfFilter,     pdf);
    private FileNameExtensionFilter pngExtFilter     = new FileNameExtensionFilter(pngFilter,     png);
    private FileNameExtensionFilter svgExtFilter     = new FileNameExtensionFilter(svgFilter,     svg);
    private FileNameExtensionFilter csvExtFilter     = new FileNameExtensionFilter(cvsFilter,     cvs);

    private static File currentDirectory;

    private JRadioButton visibleOnlyRadioButton;

    public CustomFileChooser(){
        super();
        if(currentDirectory != null){
            setCurrentDirectory(currentDirectory);

        } else {
            setCurrentDirectory(new File(System.getProperty("user.home")));
        }
    }

    public FileProperties showOpenGraphDialog(){
        setFileFilter(gdfExtFilter);
        setFileFilter(gefxExtFilter);
        setFileFilter(vnaExtFilter);
        setFileFilter(graphMLExtFilter);
        setFileFilter(gmlExtFilter);
        setFileFilter(gephiExtFilter);

        int result = showOpenDialog(null);
        currentDirectory = getCurrentDirectory();
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            String path = selectedFile.getAbsolutePath();
            if(path.endsWith(gdfExt) || path.endsWith(gefxExt) ||
                    path.endsWith(vnaExt) || path.endsWith(graphMLExt) || path.endsWith(gmlExt)|| path.endsWith(gephiExt)){

                String type = selectedFile.getName().substring(selectedFile.getName().lastIndexOf("."));
                return new FileProperties(selectedFile.getName(), selectedFile.getAbsolutePath(), type);
            }else {
                JOptionPane.showMessageDialog(this.getParent(), bundle.getString("Components.importErrorMessage"));
            }
        }
        return null;
    }


    public String showOpenCSVDialog(){
        setFileFilter(csvExtFilter);
        setAcceptAllFileFilterUsed(false);

        int result = showOpenDialog(null);
        currentDirectory = getCurrentDirectory();
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            return selectedFile.getAbsolutePath();
        }
        return null;
    }

    public String showExportCVSDialog(){
        setFileFilter(csvExtFilter);
        setAcceptAllFileFilterUsed(false);
        setDialogTitle(bundle.getString("Components.export"));

        int result = showSaveDialog(null);
        currentDirectory = getCurrentDirectory();
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            return selectedFile.getAbsolutePath();
        }
        return null;
    }

    public FileProperties showSaveAsDialog(){
        setFileFilter(gephiExtFilter);
        setAcceptAllFileFilterUsed(false);
        setDialogTitle(bundle.getString("Components.saveAs"));

        int result = showSaveDialog(null);
        currentDirectory = getCurrentDirectory();
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            String path = selectedFile.getAbsolutePath();
            if(!path.endsWith(gephiExt)){
                path += gephiExt;
            }
            return new FileProperties(selectedFile.getName(), path, gephiExt);
        }
        return null;
    }

    public FileProperties showExportGraphDialog(){
        initOptions();
        setFileFilter(gdfExtFilter);
        setFileFilter(gefxExtFilter);
        setFileFilter(vnaExtFilter);
        setFileFilter(graphMLExtFilter);
        setFileFilter(gmlExtFilter);
        setAcceptAllFileFilterUsed(false);
        setDialogTitle(bundle.getString("Components.export"));

        int result = showSaveDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            String path = selectedFile.getAbsolutePath();
            String filter = getFileFilter().getDescription();
            String extension = null;
            switch (filter) {
                case gefxFilter    : path += gefxExt;
                    extension = gefxExt;
                    break;
                case gmlFilter     : path += gmlExt;
                    extension = gmlExt;
                    break;
                case gdfFilter     : path += gdfExt;
                    extension = gdfExt;
                    break;
                case graphMLFilter : path += graphMLExt;
                    extension = graphMLExt;
                    break;
                case vnaFilter     : path += vnaExtFilter;
                    extension = vnaExt;
                    break;
            }
            return new FileProperties(selectedFile.getName(), path, extension);
        }
        return null;
    }

    public FileProperties showExportAsImageDialog(){
        setFileFilter(pdfExtFilter);
        setFileFilter(pngExtFilter);
        setFileFilter(svgExtFilter);
        setAcceptAllFileFilterUsed(false);
        setDialogTitle(bundle.getString("Components.export"));

        int result = showSaveDialog(null);
        currentDirectory = getCurrentDirectory();
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = getSelectedFile();
            String path = selectedFile.getAbsolutePath();
            String filter = getFileFilter().getDescription();
            String extension = null;

            switch (filter){
                case pdfFilter : path += pdfExt;
                    extension = pdfExt;
                    break;
                case pngFilter : path += pngExt;
                    extension = pngExt;
                    break;
                case svgFilter : path += svgExt;
                    extension = svgExt;
                    break;
            }
            return new FileProperties(selectedFile.getName(), path, extension);
        }
        return null;
    }

    private void initOptions(){
        JPanel      panel                 = new JPanel();
        JPanel      optionPanel           = new JPanel();
        JPanel      graphPanel            = new JPanel();
        JPanel      fullPanel             = new JPanel();
        JPanel      visiblePanel          = new JPanel();
        JLabel      graphLabel            = new JLabel();
        JLabel      fullGraphLabel        = new JLabel();
        JLabel      visibleOnlyLabel      = new JLabel();
        ButtonGroup buttonGroup           = new ButtonGroup();
        JRadioButton fullGraphRadioButton = new JRadioButton();
        visibleOnlyRadioButton            = new JRadioButton();

        buttonGroup.add(fullGraphRadioButton);
        buttonGroup.add(visibleOnlyRadioButton);

        graphLabel.setText(bundle.getString("Components.graph"));
        fullGraphRadioButton.setText(bundle.getString("Components.full"));
        visibleOnlyRadioButton.setText(bundle.getString("Components.visibleOnly"));
        fullGraphLabel.setText(bundle.getString("Components.fullGraphLabel"));
        visibleOnlyLabel.setText(bundle.getString("Components.visibleOnlyLabel"));

        fullGraphLabel.setForeground(new Color(120, 120, 120));
        visibleOnlyLabel.setForeground(new Color(120, 120, 120));

        fullGraphRadioButton.setSelected(true);
        graphLabel.setHorizontalAlignment(JLabel.LEFT);
        graphLabel.setVerticalAlignment(JLabel.TOP);

        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 10));
        graphPanel.setLayout(new BorderLayout());
        optionPanel.setLayout(new VerticalLayout());
        fullPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        visiblePanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

        fullPanel.add(fullGraphRadioButton);
        fullPanel.add(fullGraphLabel);

        visiblePanel.add(visibleOnlyRadioButton);
        visiblePanel.add(visibleOnlyLabel);

        optionPanel.add(fullPanel);
        optionPanel.add(visiblePanel);

        panel.add(graphLabel);

        graphLabel.setVerticalAlignment(SwingConstants.TOP);
        panel.add(optionPanel);
        for (Component component : getComponents()){
            if(component.getClass() != JPanel.class){
                    ((JPanel) component).add(panel, BorderLayout.SOUTH);
            }
        }
    }

    public boolean isVisibleOnly(){
        return visibleOnlyRadioButton.isSelected();
    }

    private static final String cvs      = "csv";
    private static final String vna      = "vna";
    private static final String gdf      = "gdf";
    private static final String gefx     = "gefx";
    private static final String gml      = "gml";
    private static final String graphML  = "graphml";
    private static final String pdf      = "pdf";
    private static final String png      = "png";
    private static final String svg      = "svg";
    private static final String gephi    = "gephi";

    private static final String vnaExt      = "." + vna;
    private static final String gdfExt      = "." + gdf;
    private static final String gefxExt     = "." + gefx;
    private static final String gmlExt      = "." + gml;
    private static final String graphMLExt  = "." + graphML;
    private static final String pdfExt      = "." + pdf;
    private static final String pngExt      = "." + png;
    private static final String svgExt      = "." + svg;
    private static final String gephiExt    = "." + gephi;

    private static final String cvsFilter     = "CSV files (*.csv)";
    private static final String vnaFilter     = "VNA files (*.vna)";
    private static final String gdfFilter     = "GDF files (*.gdf)";
    private static final String gefxFilter    = "GEFX files (*.gexf)";
    private static final String gmlFilter     = "GML files (*.gml)";
    private static final String graphMLFilter = "GraphML files (*.graphml)";
    private static final String pdfFilter     = "PDF files (*.pdf)";
    private static final String pngFilter     = "PNG files (*.png)";
    private static final String svgFilter     = "SVG files (*.svg)";
    private static final String gephiFilter   = "Gephi files (*.gephi)";

}
