package gui.components;

import controller.visualization.GephiInitializer;
import controller.visualization.Group;
import gui.components.dialogs.RenameDialog;
import org.gephi.graph.api.Node;

import javax.swing.*;
import java.util.ResourceBundle;

import static controller.visualization.GephiInitializer.CLUSTER;

/**
 * @author Gewrgia
 */

public class NodePopupMenu extends JPopupMenu {

    public NodePopupMenu(Node node){
        ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Components");

        if (Group.getPartitionMap().containsKey(node.getLabel())&& Group.getPartitionMap().get(node.getAttribute(CLUSTER).toString())){
           if(!node.getLabel().equals(GephiInitializer.INDEPENDENT)){
               JMenuItem renameMenuItem = new JMenuItem(bundle.getString("Components.renameCluster"));
               renameMenuItem.addActionListener(e ->{
                   new RenameDialog(node);
               });

               add(renameMenuItem);
           }
            JMenuItem expandMenuItem = new JMenuItem(bundle.getString("Components.expandCluster"));
            expandMenuItem.addActionListener(
                    event -> Group.preserveAppearance(
                            () -> Group.expandPartition(node)
                    )
            );
            add(expandMenuItem);

        } else if (!Group.getPartitionMap().get(node.getAttribute(CLUSTER).toString())) {
            JMenuItem groupMenuItem = new JMenuItem(bundle.getString("Components.groupCluster"));
            groupMenuItem.addActionListener(
                    event -> Group.preserveAppearance(
                            () -> Group.groupPartition(node.getAttribute(CLUSTER).toString())
                    )
            );
            add(groupMenuItem);
        }
    }

}
