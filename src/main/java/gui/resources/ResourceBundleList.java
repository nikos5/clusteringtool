package gui.resources;

import java.util.*;

/**
 * @author Gewrgia
 */

public class ResourceBundleList {

    public static String[] getPropertyStringArray(ResourceBundle bundle, String keyPrefix) {
        String[] result;
        Enumeration<String> keys = bundle.getKeys();
        List<String> temp = new LinkedList<>();

        // get the keys and add them in a temporary ArrayList
        for (; keys.hasMoreElements(); ) {
            String key = keys.nextElement();
            if (key.startsWith(keyPrefix)) {
                temp.add(key);
            }
        }
        // create a string array based on the size of temporary ArrayList
        result = new String[temp.size()];
        java.util.Collections.sort(temp);

        // store the bundle Strings in the StringArray
        for (int i = 0; i < temp.size(); i++) {
            result[i] = bundle.getString(temp.get(i));
        }

        return result;
    }

}
