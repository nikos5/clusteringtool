package gui.appearance;

import controller.visualization.Appearance;
import gui.components.CustomColorPicker;
import gui.resources.ResourceBundleList;
import org.gephi.appearance.plugin.palette.PaletteManager;
import org.gephi.appearance.plugin.palette.Preset;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Georgia Grigoriadou
 */

class NodeColorPanel extends AppearancePanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Appearance");

    private JPanel            uniqueOptionPanel;
    private JPanel            clusterOptionPanel;
    private JPanel            rankingOptionPanel;
    private JPanel            rankingColorPanel;
    private JRadioButton      uniqueRadioButton;
    private JRadioButton      clusterRadioButton;
    private JRadioButton      rankingRadioButton;
    private JLabel            colorLabel;
    private JComboBox<String> paletteComboBox;
    private JComboBox<String> degreeComboBox;
    private JButton           colorButton;
    private JButton           rankingColorButton1;
    private JButton           rankingColorButton2;

    private Color uniqueNodeColor    = Color.BLACK;
    private Color rankingNodeColor1  = Color.WHITE;
    private Color rankingNodeColor2  = Color.BLACK;

     private SwingWorker worker;

    NodeColorPanel() {
        super();
        initComponents();
        worker = new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                Appearance.setClusterNodeColor(String.valueOf(paletteComboBox.getSelectedItem()));
                return null;
            }
        };
    }

    private void initComponents() {

        uniqueOptionPanel   = new JPanel();
        clusterOptionPanel  = new JPanel();
        rankingOptionPanel  = new JPanel();
        rankingColorPanel   = new JPanel();
        uniqueRadioButton   = new JRadioButton();
        clusterRadioButton  = new JRadioButton();
        rankingRadioButton  = new JRadioButton();
        colorLabel          = new JLabel();
        paletteComboBox     = new JComboBox<>();
        degreeComboBox      = new JComboBox<>();
        colorButton         = new JButton();
        rankingColorButton1 = new JButton();
        rankingColorButton2 = new JButton();

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(uniqueRadioButton);
        buttonGroup.add(clusterRadioButton);
        buttonGroup.add(rankingRadioButton);

        uniqueRadioButton.setText(bundle.getString("Appearance.unique"));
        clusterRadioButton.setText(bundle.getString("Appearance.cluster"));
        rankingRadioButton.setText(bundle.getString("Appearance.ranking"));
        colorLabel.setText("#ffffff");

        colorButton.setBackground(Color.BLACK);
        rankingColorButton1.setBackground(Color.WHITE);
        rankingColorButton2.setBackground(Color.BLACK);

        colorButton.setPreferredSize(new Dimension(16, 16));
        rankingColorButton1.setPreferredSize(new Dimension(16, 16));
        rankingColorButton2.setPreferredSize(new Dimension(16, 16));


        colorButton.setUI(new BasicButtonUI());
        rankingColorButton1.setUI(new BasicButtonUI());
        rankingColorButton2.setUI(new BasicButtonUI());

        for (Preset preset : PaletteManager.getInstance().getPresets()) {
            paletteComboBox.addItem(preset.getName());
        }
        for (String entry : ResourceBundleList.getPropertyStringArray(bundle, "Appearance.degreeOptions")) {
            degreeComboBox.addItem(entry);
        }

        addComponents();
        addListeners();

        uniqueOptionPanel.setVisible(false);
        clusterOptionPanel.setVisible(false);
        rankingOptionPanel.setVisible(false);
    }

    private void addComponents(){

        uniqueOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        clusterOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        rankingOptionPanel.setLayout(new VerticalLayout());
        rankingColorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        panel.add(uniqueRadioButton);
        uniqueOptionPanel.add(colorButton);
        uniqueOptionPanel.add(colorLabel);
        panel.add(uniqueOptionPanel);
        panel.add(clusterRadioButton);
        clusterOptionPanel.add(paletteComboBox);
        panel.add(clusterOptionPanel);
        panel.add(rankingRadioButton);
        rankingOptionPanel.add(degreeComboBox);
        rankingColorPanel.add(rankingColorButton1);
        rankingColorPanel.add(rankingColorButton2);
        rankingOptionPanel.add(rankingColorPanel);
        panel.add(rankingOptionPanel);
    }

    private void addListeners() {
        uniqueRadioButton.addActionListener(e -> {
            if(enableAutoTransformation) {
                Appearance.setUniqueNodeColor(uniqueNodeColor);
                }
            uniqueOptionPanel.setVisible(true);
            clusterOptionPanel.setVisible(false);
            rankingOptionPanel.setVisible(false);
        });
        clusterRadioButton.addActionListener(e -> {
            if(enableAutoTransformation) {
                worker = getWorker();
                worker.execute();
            }
            uniqueOptionPanel.setVisible(false);
            clusterOptionPanel.setVisible(true);
            rankingOptionPanel.setVisible(false);
        });
        rankingRadioButton.addActionListener(e -> {
            if(enableAutoTransformation) {
                Appearance.setRankingNodeColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingNodeColor1, rankingNodeColor2);
            }
            uniqueOptionPanel.setVisible(false);
            clusterOptionPanel.setVisible(false);
            rankingOptionPanel.setVisible(true);
        });
        colorButton.addActionListener(e ->
                new CustomColorPicker(true, false,
                        color -> {
                                if (enableAutoTransformation) {
                                    Appearance.setUniqueNodeColor(color);
                                }
                                colorButton.setBackground(color);
                                colorLabel.setText("#" + Integer.toHexString(color.getRGB()).substring(2));
                                uniqueNodeColor = color;
                        }));
        paletteComboBox.addActionListener(e -> {
            if (enableAutoTransformation) {
                worker = getWorker();
                worker.execute();
            }
        });
        degreeComboBox.addActionListener(e -> {
            if (enableAutoTransformation) {
                Appearance.setRankingNodeColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingNodeColor1, rankingNodeColor2);
            }
        });
        rankingColorButton1.addActionListener(e ->
                new CustomColorPicker(true, false,
                        color -> {
                            if(enableAutoTransformation) {
                                Appearance.setRankingNodeColor(String.valueOf(degreeComboBox.getSelectedItem()), color, rankingNodeColor2);
                            }
                            rankingColorButton1.setBackground(color);
                            rankingNodeColor1 = color;
                        }));
        rankingColorButton2.addActionListener(e ->
                new CustomColorPicker(true, false,
                        color -> {
                            if(enableAutoTransformation) {
                                Appearance.setRankingNodeColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingNodeColor1, color);
                            }
                            rankingColorButton2.setBackground(color);
                            rankingNodeColor2 = color;
                        }));
    }

    @Override
    void apply() {
        if(uniqueRadioButton.isSelected()){
            Appearance.setUniqueNodeColor(uniqueNodeColor);
        }
        if(clusterRadioButton.isSelected()){
            if(worker.getState() == SwingWorker.StateValue.STARTED){
                worker.cancel(true);
            }
            worker = getWorker();
            worker.execute();
        }
        if (rankingRadioButton.isSelected()) {
            Appearance.setRankingNodeColor(String.valueOf(degreeComboBox.getSelectedItem()), rankingNodeColor1, rankingNodeColor2);
        }
    }

    private SwingWorker getWorker(){
        return  new SwingWorker<Void, Void>() {
            @Override
            public Void doInBackground() {
                Appearance.setClusterNodeColor(String.valueOf(paletteComboBox.getSelectedItem()));
                return null;
            }
        };
    }

}
