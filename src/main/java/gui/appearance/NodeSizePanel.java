package gui.appearance;

import controller.visualization.Appearance;

/**
 * @author Georgia Grigoriadou
 */

class NodeSizePanel extends SizePanel {

    NodeSizePanel(){
        super();
        uniqueSizeSpinner.setValue(Appearance.nodeMinSize);
        minSizeSpinner.setValue(Appearance.nodeMinSize);
        maxSizeSpinner.setValue(Appearance.nodeMaxSize);
        addListeners();
    }

    private void addListeners() {
        uniqueRadioButton.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setUniqueNodeSize((double) uniqueSizeSpinner.getValue());
                    }
                    uniqueOptionPanel.setVisible(true);
                    rankingOptionPanel.setVisible(false);
                }
        );
        rankingRadioButton.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankNodeSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                    uniqueOptionPanel.setVisible(false);
                    rankingOptionPanel.setVisible(true);
                }
        );
        uniqueSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {

                        Appearance.setUniqueNodeSize((double) uniqueSizeSpinner.getValue());
                    }
                }
        );
        degreeComboBox.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankNodeSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
        minSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankNodeSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
        maxSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankNodeSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
    }

    @Override
    void apply() {
        if (uniqueRadioButton.isSelected()) {
            Appearance.setUniqueNodeSize((double) uniqueSizeSpinner.getValue());
        }
        if (rankingRadioButton.isSelected()) {
            Appearance.setRankNodeSize(String.valueOf(
                    degreeComboBox.getSelectedItem()),
                    (double) minSizeSpinner.getValue(),
                    (double) maxSizeSpinner.getValue());
        }
    }

}
