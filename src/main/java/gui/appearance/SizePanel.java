package gui.appearance;

import gui.resources.ResourceBundleList;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */
abstract class SizePanel extends AppearancePanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Appearance");

    private JPanel      uniqueSizePanel;
    private JPanel      degreePanel;
    private JPanel      minSizePanel;
    private JPanel      maxSizePanel;
    private ButtonGroup buttonGroup;
    private JLabel      sizeLabel;
    private JLabel      minSizeLabel;
    private JLabel      maxSizeLabel;
    JPanel              uniqueOptionPanel;
    JPanel              rankingOptionPanel;
    JRadioButton        uniqueRadioButton;
    JRadioButton        rankingRadioButton;
    JComboBox<String>   degreeComboBox;
    JSpinner            uniqueSizeSpinner;
    JSpinner            minSizeSpinner;
    JSpinner            maxSizeSpinner;

    SizePanel() {
        super();
        initComponents();
    }

    private void initComponents() {
        uniqueOptionPanel  = new JPanel();
        uniqueSizePanel    = new JPanel();
        rankingOptionPanel = new JPanel();
        degreePanel        = new JPanel();
        minSizePanel       = new JPanel();
        maxSizePanel       = new JPanel();
        buttonGroup        = new ButtonGroup();
        uniqueRadioButton  = new JRadioButton();
        rankingRadioButton = new JRadioButton();
        sizeLabel          = new JLabel();
        minSizeLabel       = new JLabel();
        maxSizeLabel       = new JLabel();
        degreeComboBox     = new JComboBox<>();
        uniqueSizeSpinner  = new JSpinner();
        minSizeSpinner     = new JSpinner();
        maxSizeSpinner     = new JSpinner();


        uniqueRadioButton.setText(bundle.getString("Appearance.unique"));
        rankingRadioButton.setText(bundle.getString("Appearance.ranking"));
        sizeLabel.setText(bundle.getString("Appearance.size"));
        minSizeLabel.setText(bundle.getString("Appearance.minSize"));
        maxSizeLabel.setText(bundle.getString("Appearance.maxSize"));

        for (String entry : ResourceBundleList.getPropertyStringArray(bundle, "Appearance.degreeOptions")) {
            degreeComboBox.addItem(entry);
        }

        setSpinners();
        addComponents();

        uniqueOptionPanel.setVisible(false);
        rankingOptionPanel.setVisible(false);
    }

    private void setSpinners(){
        SpinnerNumberModel model    = new SpinnerNumberModel(1.0, 1.0, 10000000, 1.0);
        SpinnerNumberModel minModel = new SpinnerNumberModel(1.0, 1.0, 10000000, 1.0);
        SpinnerNumberModel maxModel = new SpinnerNumberModel(4.0, 1.0, 10000000, 1.0);

        uniqueSizeSpinner.setModel(model);
        minSizeSpinner.setModel(minModel);
        maxSizeSpinner.setModel(maxModel);

        Dimension d = uniqueSizeSpinner.getPreferredSize();
        d.width     = 50;

        uniqueSizeSpinner.setPreferredSize(d);
        minSizeSpinner.setPreferredSize(d);
        maxSizeSpinner.setPreferredSize(d);
    }

    private void addComponents(){
        uniqueOptionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        uniqueSizePanel.setLayout(new FlowLayout());
        rankingOptionPanel.setLayout(new VerticalLayout());
        degreePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        minSizePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        maxSizePanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        panel.add(uniqueRadioButton);
        uniqueSizePanel.add(sizeLabel);
        uniqueSizePanel.add(uniqueSizeSpinner);
        uniqueOptionPanel.add(uniqueSizePanel);
        panel.add(uniqueOptionPanel);
        panel.add(rankingRadioButton);
        degreePanel.add(degreeComboBox);
        rankingOptionPanel.add(degreePanel);
        minSizePanel.add(minSizeLabel);
        minSizePanel.add(minSizeSpinner);
        maxSizePanel.add(maxSizeLabel);
        maxSizePanel.add(maxSizeSpinner);
        minSizePanel.add(maxSizePanel);
        rankingOptionPanel.add(minSizePanel);
        buttonGroup.add(uniqueRadioButton);
        buttonGroup.add(rankingRadioButton);
        panel.add(rankingOptionPanel);
    }

}
