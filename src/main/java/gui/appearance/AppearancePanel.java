package gui.appearance;

import gui.components.SubtleSquareBorder;
import org.jfree.ui.tabbedui.VerticalLayout;

import javax.swing.*;
import java.awt.*;

/**
 * @author Gewrgia
 */

abstract class AppearancePanel extends JPanel {
    JPanel  panel;
    boolean enableAutoTransformation = false;

    AppearancePanel(){
        super();
        setBorder(new SubtleSquareBorder(true));

        initComponents();
    }

    private void initComponents(){
        JScrollPane scrollPane = new JScrollPane();
        panel                  = new JPanel();

        setLayout(new BorderLayout());
        panel.setLayout(new VerticalLayout());

        scrollPane.setBorder(null);

        scrollPane.setViewportView(panel);
        add(scrollPane, BorderLayout.CENTER);
    }

    void enableAutoTransformation(boolean enable){
        this.enableAutoTransformation = enable;
    }

    abstract void apply();

}
