package gui.appearance;

import controller.visualization.Appearance;

/**
 * @author Georgia Grigoriadou
 */

class LabelSizePanel extends SizePanel {

    LabelSizePanel() {
        super();
        uniqueSizeSpinner.setValue(Appearance.labelMinSize);
        minSizeSpinner.setValue(Appearance.labelMinSize);
        maxSizeSpinner.setValue(Appearance.labelMaxSize);
        addListeners();
    }

    private void addListeners() {
        uniqueRadioButton.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setUniqueLabelSize((double) uniqueSizeSpinner.getValue());
                    }
                    uniqueOptionPanel.setVisible(true);
                    rankingOptionPanel.setVisible(false);
                }
        );
        rankingRadioButton.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankLabelSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                    uniqueOptionPanel.setVisible(false);
                    rankingOptionPanel.setVisible(true);
                }
        );
        uniqueSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setUniqueLabelSize((double) uniqueSizeSpinner.getValue());
                    }
                }
        );
        degreeComboBox.addActionListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankLabelSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
        minSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {
                        Appearance.setRankLabelSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
        maxSizeSpinner.addChangeListener(
                e -> {
                    if (enableAutoTransformation) {

                        Appearance.setRankLabelSize(
                                String.valueOf(degreeComboBox.getSelectedItem()),
                                (double) minSizeSpinner.getValue(),
                                (double) maxSizeSpinner.getValue());
                    }
                });
    }

    @Override
    void apply() {
        if (uniqueRadioButton.isSelected()) {
            Appearance.setUniqueLabelSize((double) uniqueSizeSpinner.getValue());
        }
        if (rankingRadioButton.isSelected()) {
            Appearance.setRankLabelSize(
                    String.valueOf(degreeComboBox.getSelectedItem()),
                    (double) minSizeSpinner.getValue(),
                    (double) maxSizeSpinner.getValue());
        }
    }

}
