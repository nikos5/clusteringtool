package gui.layout;

import com.sun.java.swing.plaf.windows.WindowsTabbedPaneUI;
import gui.resources.ResourceBundleList;

import javax.swing.*;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.synth.SynthInternalFrameUI;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

public class LayoutFrame extends JInternalFrame {
    private ResourceBundle bundle  = ResourceBundle.getBundle("Bundle/Layout");

    private JComboBox<String> chooseLayoutComboBox;
    private JTabbedPane       tabbedPane;

    public LayoutFrame(){
        setTitle(bundle.getString("Layout.name"));
        setFrameIcon(new ImageIcon(bundle.getString("Layout.layoutIconPath")));
        initComponents();
        setBorder(new BasicBorders.MarginBorder());
        setClosable(true);
        setVisible(true);

        SynthInternalFrameUI ui = (SynthInternalFrameUI) getUI();
        ui.propertyChange(new PropertyChangeEvent(this, JInternalFrame.IS_SELECTED_PROPERTY, 0, 0));
    }

    private void initComponents(){
        JPanel panel             = new JPanel();
        JPanel chooseLayoutPanel = new JPanel();
        JPanel footerPanel       = new JPanel();
        JButton resetButton      = new JButton();

        chooseLayoutComboBox     = new JComboBox<>();
        tabbedPane               = new JTabbedPane();

        for (String layout : ResourceBundleList.getPropertyStringArray(bundle, "Layout.layouts")){
            chooseLayoutComboBox.addItem(layout);
        }

        resetButton.setIcon(new ImageIcon(bundle.getString("Layout.resetIconPath")));
        resetButton.setToolTipText((bundle.getString("Layout.reset")));
        resetButton.setFocusPainted(false);
        resetButton.setMargin(new Insets(0, 0, 0, 0));
        resetButton.setContentAreaFilled(false);
        resetButton.setBorderPainted(false);
        resetButton.setOpaque(false);

        chooseLayoutComboBox.addActionListener(e->
                tabbedPane.setSelectedIndex(chooseLayoutComboBox.getSelectedIndex())
        );

        resetButton.addActionListener(e->{
            tabbedPane.removeAll();
            initTabbedPanel();
            tabbedPane.setSelectedIndex(chooseLayoutComboBox.getSelectedIndex());
        });

        initTabbedPanel();
        setLayout(new BorderLayout());
        panel.setLayout(new BorderLayout());
        chooseLayoutPanel.setLayout(new BorderLayout());
        footerPanel.setLayout(new BorderLayout());

        chooseLayoutPanel.add(chooseLayoutComboBox);
        panel.add(chooseLayoutPanel, BorderLayout.NORTH);
        panel.add(tabbedPane,BorderLayout.CENTER);
        footerPanel.add(resetButton, BorderLayout.WEST);
        panel.add(footerPanel, BorderLayout.SOUTH);
        setContentPane(panel);

        UIManager.put("TabbedPane.tabInsets", new Insets(-10, -10, -10, -10));
        tabbedPane.setUI(new WindowsTabbedPaneUI());
    }

    private void initTabbedPanel(){
        LayoutPanel clusterLayoutPanel       = new ClusterLayoutPanel();
        LayoutPanel contractPanel            = new ContractionPanel();
        LayoutPanel expandPanel              = new ExpansionPanel();
        LayoutPanel forceAtlasPanel          = new ForceAtlasPanel();
        LayoutPanel forceAtlas2Panel         = new ForceAtlas2Panel();
        LayoutPanel fruchterReinGoldPanel    = new FruchtermanReinGoldPanel();
        LayoutPanel labelAdjustPanel         = new LabelAdjustPanel();
        LayoutPanel nonoverlapPanel          = new NonoverlapPanel();
        LayoutPanel openOrdPanel             = new OpenOrdPanel();
        LayoutPanel randomPanel              = new RandomPanel();
        LayoutPanel rotatePanel              = new RotatePanel();
        LayoutPanel yifanHuPanel             = new YifanHuPanel(false);
        LayoutPanel yifanHuProportionalPanel = new YifanHuPanel(true);

        tabbedPane.add(clusterLayoutPanel,0);
        tabbedPane.add(contractPanel,1);
        tabbedPane.add(expandPanel,2);
        tabbedPane.add(forceAtlasPanel,3);
        tabbedPane.add(forceAtlas2Panel,4);
        tabbedPane.add(fruchterReinGoldPanel,5);
        tabbedPane.add(labelAdjustPanel,6);
        tabbedPane.add(nonoverlapPanel,7);
        tabbedPane.add(openOrdPanel,8);
        tabbedPane.add(randomPanel,9);
        tabbedPane.add(rotatePanel,10);
        tabbedPane.add(yifanHuPanel,11);
        tabbedPane.add(yifanHuProportionalPanel,12);

        for (Component c: tabbedPane.getComponents()){
            if(LayoutPanel.class.isAssignableFrom(c.getClass())) {
                ((LayoutPanel) c).startButton.addPropertyChangeListener("visible", e -> chooseLayoutComboBox.setEnabled( (boolean )e.getNewValue()));
            }
        }
    }

    public void reset(){
        tabbedPane.removeAll();
        initTabbedPanel();
        chooseLayoutComboBox.setSelectedIndex(0);
    }

    public void setVisible(boolean b) {
        boolean visible = isVisible();
        super.setVisible(b);
        firePropertyChange("visible", visible, b);
    }

}
