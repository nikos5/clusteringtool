package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class LabelAdjustPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/LabelAdjust");

    private JTable table;

    LabelAdjustPanel(){
        super();

        initTable();
        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();

                        double speed = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        boolean includeNodeSize = (boolean) table.getModel().getValueAt(1, 1);

                        startLayout(Layout.getLabelAdjustLayout(speed, includeNodeSize));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("description"));
    }

    private void initTable(){
        table = new CustomTable(){
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("LabelAdjust.speed.desc");
                            break;
                        case 1:
                            tip = bundle.getString("LabelAdjust.adjustBySize.desc");
                            break;
                    }
                }
                return tip;
            }
        };

        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row  = new Object[2];
        row[0]        = bundle.getString("LabelAdjust.speed.name");
        row[1]        = "0.8";
        Object[] row1 = new Object[2];
        row1[0]       = bundle.getString("LabelAdjust.adjustBySize.name");
        row1[1]       = true;

        model.addRow(row);
        model.addRow(row1);

        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("name"), table);
        tablePanel.add(collapsibleTable);
    }

}
