package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class RotatePanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Rotate");

    private JTable table;

    RotatePanel() {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                        if (table.isEditing()) table.getCellEditor().stopCellEditing();
                        double angle = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                        startLayout(Layout.getRotateLayout(angle));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("rotate.description"));
    }

    private void initTable() {
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                return colIndex == 1 ? getValueAt(rowIndex, colIndex).toString(): bundle.getString("rotate.angle.desc");
            }
        };

        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row = new Object[2];
        row[0]       = bundle.getString("rotate.angle.name");
        row[1]       = "90.0";

        model.addRow(row);
        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("rotate.properties"), table);
        tablePanel.add(collapsibleTable);
    }

}
