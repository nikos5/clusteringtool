package gui.layout;

import controller.visualization.Layout;
import gui.components.CollapsibleTable;
import gui.components.CustomTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

class ForceAtlasPanel extends LayoutPanel {
    private ResourceBundle bundle = ResourceBundle.getBundle("Bundle/ForceAtlas");

    private JTable table;

    ForceAtlasPanel() {
        super();

        initTable();

        startButton.addActionListener(
                e -> {
                    try {
                         if (table.isEditing()) table.getCellEditor().stopCellEditing();
                         double[] doubleValues   = new double[8];
                         boolean[] booleanValues = new boolean[3];

                         doubleValues[0]  = Double.parseDouble((String) table.getModel().getValueAt(0, 1));
                         doubleValues[1]  = Double.parseDouble((String) table.getModel().getValueAt(1, 1));
                         doubleValues[2]  = Double.parseDouble((String) table.getModel().getValueAt(2, 1));
                         doubleValues[3]  = Double.parseDouble((String) table.getModel().getValueAt(3, 1));
                         doubleValues[4]  = Double.parseDouble((String) table.getModel().getValueAt(5, 1));
                         doubleValues[5]  = Double.parseDouble((String) table.getModel().getValueAt(6, 1));
                         doubleValues[6]  = Double.parseDouble((String) table.getModel().getValueAt(7, 1));
                         doubleValues[7]  = Double.parseDouble((String) table.getModel().getValueAt(10, 1));
                         booleanValues[0] = (boolean) table.getModel().getValueAt(4, 1);
                         booleanValues[1] = (boolean) table.getModel().getValueAt(8, 1);
                         booleanValues[2] = (boolean) table.getModel().getValueAt(9, 1);

                        startLayout(Layout.getForceAtlasLayout(doubleValues, booleanValues));
                    } catch (NumberFormatException ex) {
                        showErrorMessage(ex);
                    }
                }
        );
        infoLabel.setToolTipText(bundle.getString("description"));
    }

    private void initTable() {
        table = new CustomTable() {
            //Implement stagedTable cell tool tips.
            public String getToolTipText(MouseEvent e) {
                String tip   = null;
                Point p      = e.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                if (colIndex == 1) {
                    tip = getValueAt(rowIndex, colIndex).toString();
                } else {
                    switch (rowIndex) {
                        case 0:
                            tip = bundle.getString("forceAtlas.inertia.desc");
                            break;
                        case 1:
                            tip = bundle.getString("forceAtlas.repulsionStrength.desc");
                            break;
                        case 2:
                            tip = bundle.getString("forceAtlas.attractionStrength.desc");
                            break;
                        case 3:
                            tip = bundle.getString("forceAtlas.maxDisplacement.desc");
                            break;
                        case 4:
                            tip = bundle.getString("forceAtlas.freezeBalance.desc");
                            break;
                        case 5:
                            tip = bundle.getString("forceAtlas.freezeStrength.desc");
                            break;
                        case 6:
                            tip = bundle.getString("forceAtlas.freezeInertia.desc");
                            break;
                        case 7:
                            tip = bundle.getString("forceAtlas.gravity.desc");
                            break;
                        case 8:
                            tip = bundle.getString("forceAtlas.outboundAttractionDistribution.desc");
                            break;
                        case 9:
                            tip = bundle.getString("forceAtlas.adjustSizes.desc");
                            break;
                        case 10:
                            tip = bundle.getString("forceAtlas.speed.desc");
                            break;
                    }
                }
                return tip;
            }
        };
        DefaultTableModel model = (DefaultTableModel) table.getModel();

        Object[] row   = new Object[2];
        row[0]         = bundle.getString("forceAtlas.inertia.name");
        row[1]         = "0.1";
        Object[] row1  = new Object[2];
        row1[0]        = bundle.getString("forceAtlas.repulsionStrength.name");
        row1[1]        = "200.0";
        Object[] row2  = new Object[2];
        row2[0]        = bundle.getString("forceAtlas.attractionStrength.name");
        row2[1]        = "10.0";
        Object[] row3  = new Object[2];
        row3[0]        = bundle.getString("forceAtlas.maxDisplacement.name");
        row3[1]        = "10.0";
        Object[] row4  = new Object[2];
        row4[0]        = bundle.getString("forceAtlas.freezeBalance.name");
        row4[1]        = true;
        Object[] row5  = new Object[2];
        row5[0]        = bundle.getString("forceAtlas.freezeStrength.name");
        row5[1]        = "80.0";
        Object[] row6  = new Object[2];
        row6[0]        = bundle.getString("forceAtlas.freezeInertia.name");
        row6[1]        = "0.2";
        Object[] row7  = new Object[2];
        row7[0]        = bundle.getString("forceAtlas.gravity.name");
        row7[1]        = "30.0";
        Object[] row8  = new Object[2];
        row8[0]        = bundle.getString("forceAtlas.outboundAttractionDistribution.name");
        row8[1]        = false;
        Object[] row9  = new Object[2];
        row9[0]        = bundle.getString("forceAtlas.adjustSizes.name");
        row9[1]        = false;
        Object[] row10 = new Object[2];
        row10[0]       = bundle.getString("forceAtlas.speed.name");
        row10[1]       = "1.0";

        model.addRow(row);
        model.addRow(row1);
        model.addRow(row2);
        model.addRow(row3);
        model.addRow(row4);
        model.addRow(row5);
        model.addRow(row6);
        model.addRow(row7);
        model.addRow(row8);
        model.addRow(row9);
        model.addRow(row10);

        CollapsibleTable collapsibleTable = new CollapsibleTable(bundle.getString("name"), table);
        tablePanel.add(collapsibleTable);
    }

}
