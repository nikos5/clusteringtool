package gui.dataLaboratory;

import controller.visualization.GephiInitializer;
import gui.resources.ResourceBundleList;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.EdgeIterable;
import controller.Project;

/**
 * @author Gewrgia
 */

class EdgesTablePanel extends TablePanel{

    EdgesTablePanel(){}

    @Override
    void init() {
        initTable( () -> {
            createTableModel(ResourceBundleList.getPropertyStringArray(bundle, "Table.edgeColumns"));

            EdgeIterable edges = Project.getGraphModel().getNodeTable().getGraph().getEdges();
            for (Edge edge : edges) {
                Object[] row = new Object[6];
                row[0] = edge.getSource().getLabel();
                row[1] = edge.getTarget().getLabel();
                if( edge.isDirected()) {
                    row[2] = bundle.getString("Table.directed");
                } else {
                    row[2] = bundle.getString("Table.undirected");
                }
                row[3] = edge.getWeight();
                row[4] = edge.getAttribute(GephiInitializer.INTRA_EDGE);
                row[5] = edge.getAttribute(GephiInitializer.INTER_EDGE);

                tableModel.addRow(row);
            }
        });
    }

    @Override
    void rename(String oldID, String newID) {}

}
