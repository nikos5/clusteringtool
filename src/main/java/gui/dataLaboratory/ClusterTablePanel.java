package gui.dataLaboratory;

import gui.resources.ResourceBundleList;
import model.api.Cluster;
import model.api.MDG;
import controller.Project;

/**
 * @author Gewrgia
 */

class ClusterTablePanel extends TablePanel {

    ClusterTablePanel(){}

    @Override
    void init(){

        initTable( () -> {
            createTableModel(ResourceBundleList.getPropertyStringArray(bundle, "Table.clusterColumns"));

            MDG mdg = Project.getMdg();
            for (Cluster cluster : mdg.getClusters()) {
                Object[] row = new Object[5];
                row[0] = cluster.getId();
                row[1] = cluster.getNodes().size();
                row[2] = cluster.getClusterFactor();
                row[3] = cluster.getIntraEdges();
                row[4] = cluster.getInterEdges();

                tableModel.addRow(row);
            }
        });
    }

    @Override
    void rename(String oldID, String newID) {
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            String oldValue = (String) tableModel.getValueAt(i, 0);
            if (oldValue.equals(oldID)) {
                tableModel.setValueAt(newID, i, 0);
            }
        }
    }

}
