package gui.dataLaboratory;

import gui.components.CustomFileChooser;
import gui.components.DropDownButton;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Consumer;

/**
 * @author Gewrgia
 */
abstract class TablePanel extends JPanel {
     ResourceBundle bundle = ResourceBundle.getBundle("Bundle/Table");

    private JPanel            panel;
    private JScrollPane       scrollPane;
    private TableRowSorter    rowSorter;
    private JTextField        filterTextField;
    private JTable            table;
    private JComboBox<String> filterColumnComboBox;
    private DropDownButton    hideColumnsMenu;

    DefaultTableModel tableModel;

    TablePanel(){
        initComponents();
    }

    abstract void init();

    abstract void rename(String oldID, String newID);

    private void initComponents(){
        JPanel leftPanel     = new JPanel();
        JToolBar toolBar     = new JToolBar();
        JToolBar footer      = new JToolBar();
        JLabel filterLabel   = new JLabel();
        JButton exportButton = new JButton();

        panel                = new JPanel();
        scrollPane           = new JScrollPane();
        filterTextField      = new JTextField();
        filterColumnComboBox = new JComboBox<>();
        hideColumnsMenu      = new DropDownButton();

        filterLabel.setText(bundle.getString("Table.filter"));
        exportButton.setText(bundle.getString("Table.export"));

        filterTextField.setPreferredSize(new Dimension(100,20));
        filterTextField.getDocument().addDocumentListener(
                new DocumentListener() {
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }
                }
        );

        hideColumnsMenu.setPreferredSize(new Dimension(25,20));
        hideColumnsMenu.setMargin(new Insets(2,0,0,0));
        hideColumnsMenu.setIcon(new ImageIcon(bundle.getString("Table.menuIconPath")));

        exportButton.setForeground(Color.BLUE);
        exportButton.setIcon(new ImageIcon(bundle.getString("Table.saveIconPath")));
        exportButton.addActionListener((e) -> exportCVS());

        footer.setFloatable(false);
        footer.setPreferredSize(new Dimension((int) getPreferredSize().getWidth(), 30));
        footer.setLayout(new FlowLayout(FlowLayout.LEFT));
        footer.add(exportButton);

        setLayout(new BorderLayout());
        panel.setLayout(new BorderLayout());

        toolBar.setLayout(new BorderLayout());

        leftPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 2, 2));
        leftPanel.add(filterLabel);
        leftPanel.add(filterTextField);
        leftPanel.add(filterColumnComboBox);
        leftPanel.add(hideColumnsMenu);

        toolBar.add(leftPanel, BorderLayout.EAST);
        toolBar.setFloatable(false);

        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(scrollPane, BorderLayout.CENTER);
        panel.add(footer, BorderLayout.SOUTH);

        add(panel);
    }

	@SuppressWarnings("unchecked")
    void initTable(Runnable runnable) {
        table      = new JTable() {
            @Override
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        tableModel = new DefaultTableModel();

        runnable.run();
        rowSorter = new TableRowSorter(tableModel);
        rowSorter.setStringConverter(new TableStringConverter() {
            @Override
            public String toString(TableModel model, int row, int column) {
                return model.getValueAt(row, column).toString().toLowerCase();
            }
        });
        table.setAutoCreateRowSorter(true);
        table.setFillsViewportHeight(true);
        table.setRowSorter(rowSorter);

        table.setBackground(Color.WHITE);
        scrollPane.setViewportView(table);
    }

    void reset(){
        table.setVisible(false);
        table      = null;
        tableModel = null;
        hideColumnsMenu.removeAll();
        filterColumnComboBox.removeAllItems();
    }

    void createTableModel(String[] strings){
        Map<String, Consumer> hideColumns = new LinkedHashMap<>();
        for (String columnName : strings) {
            tableModel.addColumn(columnName);
            filterColumnComboBox.addItem(columnName);
            hideColumns.put(columnName,hideMenuItemListener);
        }
        hideColumnsMenu.setDropDownOptions( hideColumns, true);
        table.setModel(tableModel);

        for (int i = 0; i < strings.length; i++) {
            TableColumn column = table.getColumnModel().getColumn(i);
            column.setIdentifier(column.getHeaderValue());
        }
    }

    private Consumer<ActionEvent> hideMenuItemListener = (e) -> {
        JCheckBoxMenuItem item = (JCheckBoxMenuItem) e.getSource();
        TableColumnModel tcm = table.getColumnModel();
        TableColumn column = tcm.getColumn(tcm.getColumnIndex(item.getText()));
        if (!item.getState()) {
            column.setMinWidth(0);
            column.setMaxWidth(0);
            column.setWidth(0);
            column.setPreferredWidth(0);
        } else {
            column.setMinWidth(15);
            column.setMaxWidth(panel.getWidth());
            column.setWidth(250);
            column.setPreferredWidth(250);
        }
        doLayout();
    };

    @SuppressWarnings("unchecked")
    private void newFilter() {
        RowFilter<DefaultTableModel, Object> rowFilter;
        try {
            rowFilter = RowFilter.regexFilter(filterTextField.getText().toLowerCase(), filterColumnComboBox.getSelectedIndex());
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        }
        rowSorter.setRowFilter(rowFilter);
    }

    private void exportCVS(){
        CustomFileChooser fileChooser = new CustomFileChooser();
        String path = fileChooser.showExportCVSDialog();
        if(path != null){
            FileWriter fileWriter = null;
            String COMMA_DELIMITER = ",";
            String NEW_LINE_SEPARATOR = "\n";
            String FILE_HEADER = "";
            for (int i=0;i < table.getColumnModel().getColumnCount(); i++){
                TableColumn column = table.getColumnModel().getColumn(i);
                FILE_HEADER += column.getHeaderValue() + COMMA_DELIMITER;
            }

            try {
                fileWriter = new FileWriter(path + ".csv");
                fileWriter.append(FILE_HEADER);
                fileWriter.append(NEW_LINE_SEPARATOR);

                for (int i=0; i < tableModel.getRowCount(); i++) {
                    for (int j = 0; j < tableModel.getColumnCount(); j++) {

                        fileWriter.append(tableModel.getValueAt(i, j).toString());
                        fileWriter.append(COMMA_DELIMITER);
                    }

                    fileWriter.append(NEW_LINE_SEPARATOR);
                }

                JOptionPane.showMessageDialog(this,bundle.getString("Table.tableExportSuccessMessage"));

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this,bundle.getString("Table.tableExportErrorMessage"));
                e.printStackTrace();
            } finally {

                try {
                    assert fileWriter != null;
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
