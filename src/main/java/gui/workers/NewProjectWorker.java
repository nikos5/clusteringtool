package gui.workers;

import controller.Project;

import javax.swing.*;

/**
 * @author Gewrgia
 */

public  class NewProjectWorker extends SwingWorker<Void, Integer> {

    private JProgressBar progressBar;
    private String[]     paths;
    private int          population;
    private boolean      weighEdges;
    private Runnable     runnable;

    public NewProjectWorker(JProgressBar jpb,String[] paths, int population, boolean weightEdges, Runnable runnable) {
        this.progressBar = jpb;
        this.paths       = paths;
        this.population  = population;
        this.weighEdges  = weightEdges;
        this.runnable    = runnable;
    }

    @Override
    protected void process(java.util.List<Integer> chunks) {
        int i = chunks.get(chunks.size()-1);
        progressBar.setValue(i);
    }

    @Override
    protected Void doInBackground() throws Exception {
        if (paths[0] != null && paths[1] != null) {
            progressBar.setValue(0);
            progressBar.setVisible(true);
            Project.initializeGraph(paths[0], paths[1], weighEdges);
            publish(5);
            Project.cluster(population);
            publish(65);
            Project.setGephi();
            publish(100);
        }
        return null;
    }

    @Override
    protected void done() {
        runnable.run();
        progressBar.setVisible(false);
    }

}
