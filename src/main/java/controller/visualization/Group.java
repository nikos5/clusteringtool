package controller.visualization;

import controller.Project;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.types.DependantOriginalColor;
import org.gephi.preview.types.EdgeColor;
import org.openide.util.Lookup;

import java.awt.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static controller.Project.getMdg;
import static controller.visualization.Appearance.*;
import static controller.visualization.GephiInitializer.*;

/**
 * @author Gewrgia
 */
@SuppressWarnings("WeakerAccess")
public class Group {

    private static Map<String, Boolean>          partitionMap ;
    private static Map<Integer, model.api.Node>  nodeMap;
    private static Map<Integer, Node>            gephiNodeMap;

    @SuppressWarnings("unchecked")
    public static void init() {
        nodeMap      = new HashMap<>();
        gephiNodeMap = new HashMap<>();
        partitionMap = new HashMap<>();

        Project.getGraph().getNodes()
                .forEach(node -> nodeMap.put(node.getId(), node));
        Project.getGraphModel().getDirectedGraph().getNodes()
                .forEach(node -> gephiNodeMap.put((int) Float.parseFloat((String) node.getId()), node));
        partition.getSortedValues()
                .forEach(par -> partitionMap.put(par.toString(), false));
    }

    @SuppressWarnings("unchecked")
    public static void groupAll() {
        partition.getSortedValues().stream()
                .filter(par -> !partitionMap.get(par.toString()))
                .forEach(par -> groupPartition(par.toString()));
    }

    public static void expandAll() {
        Project.getGraphModel().getDirectedGraph().getNodes()
                .toCollection()
                .stream()
                .filter(clNode -> partitionMap.containsKey(clNode.getLabel()) && partitionMap.get(clNode.getLabel()))
                .forEach(Group::expandPartition);
    }

    public static void groupPartition(String partition) {
        if (!partitionMap.get(partition)) {
            GraphModel graphModel = Project.getGraphModel();
            Node newNode = graphModel.factory().newNode(CLUSTER + partition);
            newNode.setLabel(partition);
            AtomicReference<Set<Float>> x = new AtomicReference<>(new HashSet<>());
            AtomicReference<Set<Float>> y = new AtomicReference<>(new HashSet<>());
            AtomicReference<Color> color = new AtomicReference<>();
            AtomicReference<Color> labelColor = new AtomicReference<>();
            Set<Node> nodes = new HashSet<>();
            Set<Edge> edges = new HashSet<>();

            DirectedGraph directedGraph = Project.getGraphModel().getDirectedGraph();

            directedGraph.getNodes().toCollection().stream()
                    .filter(node -> node.getAttribute(CLUSTER).equals(partition))
                    .forEach(
                            node -> {
                                x.get().add(node.x());
                                y.get().add(node.y());
                                color.set(node.getColor());
                                labelColor.set(node.getTextProperties().getColor());
                                nodes.add(node);
                                directedGraph.getEdges(node).toCollection().stream()
                                        .filter(edge -> !edge.getSource().getAttribute(CLUSTER).equals(edge.getTarget().getAttribute(CLUSTER)))
                                        .forEach(edge -> {
                                            Node source;
                                            Node target;

                                            if (edge.getSource() == node) {
                                                source = newNode;
                                                target = edge.getTarget();
                                            } else {
                                                source = edge.getSource();
                                                target = newNode;
                                            }
                                            Edge newEdge = graphModel.factory().newEdge(
                                                    source,
                                                    target,
                                                    0,
                                                    edge.getWeight(),
                                                    edge.isDirected()
                                            );
                                            newEdge.setAttribute(INTRA_EDGE, String.valueOf(false));
                                            newEdge.setAttribute(INTER_EDGE, String.valueOf(true));
                                            edges.add(newEdge);
                                        });
                            });
            nodes.forEach( node ->{
                directedGraph.removeAllEdges(directedGraph.getEdges(node).toCollection());
                directedGraph.removeNode(node);});

            AtomicReference<Float> newX = new AtomicReference<>((float) 0);
            AtomicReference<Float> newY = new AtomicReference<>((float) 0);
            x.get().forEach(i -> newX.set(newX.get() + i));
            y.get().forEach(i -> newY.set(newY.get() + i));
            newNode.setX(newX.get() / x.get().size());
            newNode.setY(newY.get() / y.get().size());
            newNode.setSize((float) 5);
            newNode.setColor(color.get());
            newNode.getTextProperties().setText(newNode.getLabel());
            newNode.getTextProperties().setVisible(true);
            newNode.getTextProperties().setColor(labelColor.get());
            newNode.setAttribute(CLUSTER, partition);
            directedGraph.addNode(newNode);
            edges.forEach(directedGraph::addEdge);

            partitionMap.replace(partition, true);
        }
    }

    public static void expandPartition(Node clNode) {
        DirectedGraph directedGraph = Project.getGraphModel().getDirectedGraph();

        if(partitionMap.get(clNode.getAttribute(CLUSTER).toString())) {
            partitionMap.replace(clNode.getAttribute(CLUSTER).toString(), false);
            Set<Node> nodes = new HashSet<>();
            if (clNode.getLabel().equals(GephiInitializer.INDEPENDENT)) {
                getMdg().getIndependentNodes()
                        .forEach(node ->
                                nodes.add(initNode(node, clNode, GephiInitializer.INDEPENDENT))
                        );

            } else {
                getMdg().getClusters().stream()
                        .filter(cl -> cl.getId().equals(clNode.getLabel()))
                        .forEach(cl -> cl.getNodes()
                                .forEach(node ->
                                        nodes.add(initNode(node, clNode, cl.getId())
                                        )
                                )
                        );
            }
            directedGraph.removeAllEdges(directedGraph.getEdges(clNode).toCollection());
            directedGraph.removeNode(clNode);
            nodes.forEach(directedGraph::addNode);
            nodes.forEach( node -> {
                Set<Edge> edges = new HashSet<>();
                Project.getGraph().getEdges()
                        .stream()
                        .filter(edge ->
                                edge.getSourceID() == (int) Float.parseFloat((String) node.getId()) || edge.getTargetID() == (int) Float.parseFloat((String) node.getId()))
                        .forEach(edge -> {
                            Node source = null;
                            Node target = null;
                            if (edge.getSourceID() == (int) Float.parseFloat((String) node.getId())) {
                                source = node;
                                target = gephiNodeMap.get(edge.getTargetID());
                                if(!directedGraph.contains(target)){
                                    target = directedGraph.getNode(CLUSTER + Project.getMdg().getClusterOfTargetNode(nodeMap.get(edge.getTargetID())).getId());
                                }
                            }
                            if (edge.getTargetID() ==(int) Float.parseFloat((String) node.getId())) {
                                source = gephiNodeMap.get(edge.getSourceID());
                                target = node;
                                if(!directedGraph.contains(source)){
                                    source = directedGraph.getNode(CLUSTER + Project.getMdg().getClusterOfTargetNode(nodeMap.get(edge.getSourceID())).getId());
                                }
                            }
                            if(source != null && target != null) {
                                boolean isDirected = false;

                                if (edge.getType().equals(DIRECTED)) {
                                    isDirected = true;
                                }
                                boolean intra = false;
                                if ((source.getAttribute(CLUSTER)).equals(target.getAttribute(CLUSTER))) {
                                    intra = true;
                                }
                                Edge newEdge = Project.getGraphModel().factory().newEdge(
                                        source,
                                        target,
                                        0,
                                        edge.getWeight(),
                                        isDirected
                                );
                                newEdge.setAttribute(INTRA_EDGE, String.valueOf(intra));
                                newEdge.setAttribute(INTER_EDGE, String.valueOf(!intra));
                                edges.add(newEdge);
                            }
                        });
                edges.forEach(directedGraph::addEdge);
            });
        }
    }

    public static void preserveAppearance(Runnable runnable){

        Project.getGraphModel().getGraph().readUnlockAll();
        runnable.run();

        Project.getGraphModel().setVisibleView(Project.getGraphModel().getDirectedGraph().getView());
        Filter.filter();


        preserveNodeAppearance();
        preserveLabelAppearance();
        preserveEdgeAppearance();

        Preview.refreshPreview();
    }

    public static Map<String, Boolean> getPartitionMap() {
        return partitionMap;
    }

    private static Node initNode(model.api.Node node, Node clNode, String clusterID){
        Node newNode = gephiNodeMap.get(node.getId());
        newNode.setAttribute(CLUSTER, clusterID);
        newNode.setLabel(node.getName());
        newNode.setColor(clNode.getColor());
        return newNode;
    }

    private static void preserveNodeAppearance() {
        if (Appearance.nSizeDegree)
            Appearance.setRankNodeSize(Appearance.DEGREE, Appearance.nodeMinSize, Appearance.nodeMaxSize);
        if (Appearance.nSizeInDegree)
            Appearance.setRankNodeSize(Appearance.IN_DEGREE, Appearance.nodeMinSize, Appearance.nodeMaxSize);
        if (Appearance.nSizeOutDegree)
            Appearance.setRankNodeSize(Appearance.OUT_DEGREE, Appearance.nodeMinSize, Appearance.nodeMaxSize);
        if (Appearance.nodeMinSize == Appearance.nodeMaxSize) Appearance.setUniqueNodeSize(Appearance.nodeMinSize);

        if (nColor != null) Appearance.setUniqueNodeColor(nColor);
        if (nRankingColor1 != null && nRankingColor2 != null) {
            if (nColorDegree)    Appearance.setRankingNodeColor(Appearance.DEGREE, nRankingColor1, nRankingColor2);
            if (nColorInDegree)  Appearance.setRankingNodeColor(Appearance.IN_DEGREE, nRankingColor1, nRankingColor2);
            if (nColorOutDegree) Appearance.setRankingNodeColor(Appearance.OUT_DEGREE, nRankingColor1, nRankingColor2);
        }
    }

    private static void preserveLabelAppearance() {
        if (Appearance.lSizeDegree)
            Appearance.setRankLabelSize(Appearance.DEGREE, Appearance.labelMinSize, Appearance.labelMaxSize);
        if (Appearance.lSizeInDegree)
            Appearance.setRankLabelSize(Appearance.IN_DEGREE, Appearance.labelMinSize, Appearance.labelMaxSize);
        if (Appearance.lSizeOutDegree)
            Appearance.setRankLabelSize(Appearance.OUT_DEGREE, Appearance.labelMinSize, Appearance.labelMaxSize);
        if (Appearance.labelMinSize == Appearance.labelMaxSize) Appearance.setUniqueLabelSize(Appearance.labelMinSize);

        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel           = previewController.getModel();
        if (((DependantOriginalColor) previewModel.getProperties().getValue(PreviewProperty.NODE_LABEL_COLOR)).getMode() == DependantOriginalColor.Mode.ORIGINAL) {
            if (lColor != null) Appearance.setUniqueLabelColor(lColor);
            if (lRankingColor1 != null && lRankingColor2 != null) {
                if (lColorDegree) Appearance.setRankingLabelColor(Appearance.DEGREE, lRankingColor1, lRankingColor2);
                if (lColorInDegree)
                    Appearance.setRankingLabelColor(Appearance.IN_DEGREE, lRankingColor1, lRankingColor2);
                if (lColorOutDegree)
                    Appearance.setRankingLabelColor(Appearance.OUT_DEGREE, lRankingColor1, lRankingColor2);
            }
        }
    }

    private static void preserveEdgeAppearance(){
        PreviewController previewController = Lookup.getDefault().lookup(PreviewController.class);
        PreviewModel previewModel           = previewController.getModel();
        if (((EdgeColor) previewModel.getProperties().getValue(PreviewProperty.EDGE_COLOR)).getMode() == EdgeColor.Mode.ORIGINAL) {
            if (eColor != null) Appearance.setUniqueEdgeColor(eColor);
            if (eRankingColor1 != null && eRankingColor2 != null)
                Appearance.setRankingEdgeColor(eRankingColor1, eRankingColor2);
            if (intraColor != null && interColor != null) {
                boolean intraExist = false;
                boolean interExist = false;
                for (Edge edge : Project.getGraphModel().getDirectedGraph().getEdges().toCollection()) {
                    if (Boolean.parseBoolean((String) edge.getAttribute(INTRA_EDGE))) {
                        intraExist = true;
                        if (interExist) break;
                    }
                    if (Boolean.parseBoolean((String) edge.getAttribute(INTER_EDGE))) {
                        interExist = true;
                        if (intraExist) break;
                    }
                }
                if (intraExist && interExist) {
                    Appearance.setInterIntraEdgeColor(intraColor, interColor);
                } else {
                    Color color1 = intraColor;
                    Color color2 = interColor;
                    if (intraExist) {
                        Appearance.setUniqueEdgeColor(intraColor);
                    } else if (interExist) {
                        Appearance.setUniqueEdgeColor(interColor);
                    }
                    eColor         = null;
                    eRankingColor1 = null;
                    eRankingColor2 = null;
                    intraColor     = color1;
                    interColor     = color2;
                }
            }
        }
    }

}


