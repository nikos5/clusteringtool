package controller.visualization.plugins.layout;

import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutUI;

import javax.swing.*;
import java.util.ResourceBundle;

/**
 * @author Gewrgia
 */

public class ClusterLayoutBuilder implements LayoutBuilder {
        private final ClusterLayoutBuilder.ClusterLayoutUI ui = new ClusterLayoutBuilder.ClusterLayoutUI();
        private static final ResourceBundle bundle = ResourceBundle.getBundle("Bundle/ClusterLayout");

        public ClusterLayoutBuilder() {
        }

        public String getName() {
            return bundle.getString("name");
        }

        public LayoutUI getUI() {
            return this.ui;
        }

        public Layout buildLayout() {
            return new ClusterLayout(this);
        }

        private static class ClusterLayoutUI implements LayoutUI {
            private ClusterLayoutUI() {
            }

            public String getDescription() {
                return bundle.getString("description");
            }

            public Icon getIcon() {
                return null;
            }

            public JPanel getSimplePanel(Layout layout) {
                return null;
            }

            public int getQualityRank() {
                return -1;
            }

            public int getSpeedRank() {
                return -1;
            }
        }

}
