package controller;

import controller.clustering.HillClimb;
import controller.visualization.Export;
import controller.visualization.GephiInitializer;
import gui.SheafFrame;
import gui.resources.FileProperties;
import gui.resources.History;
import model.api.Cluster;
import model.api.Graph;
import model.api.MDG;
import model.impl.ClusterImpl;
import model.impl.MDGImpl;
import org.gephi.graph.api.DirectedGraph;
import org.gephi.graph.api.GraphModel;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.gephi.project.io.SaveTask;
import org.openide.util.Lookup;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Georgia Grigoriadou
 */

public class Project {
    private static Graph                graph;
    private static MDG                  mdg;
    private static Workspace            workspace;
    private static GraphModel           graphModel;
    private static boolean              saved;
    private static FileProperties       fileProperties;
    private static SheafFrame.MainPanel mainPanel;

    public Project() {
        saved = false;
    }
    public static void initializeGraph(String nodesPath, String edgesPath, boolean weightEdges) {
        GraphInitializer graphInitializer = new GraphInitializer();
        graph = graphInitializer.initialize.apply(nodesPath, edgesPath, weightEdges);
    }

    public static void initializeGraph(DirectedGraph directedGraph){
        GraphInitializer graphInitializer = new GraphInitializer();
        graph = graphInitializer.createGraph.apply(directedGraph);
    }

    public static void open(FileProperties file){
        workspace = GephiInitializer.newWorkspace();
        if(file.getLocation().endsWith(".gephi")){
            graphModel = GephiInitializer.openGephi(file.getLocation());
        } else {
            graphModel = GephiInitializer.openGraph(file.getLocation());
        }
        fileProperties = file;
        History.addRecentFile(file);
        if(Project.getMdg() == null) {
            Project.createMDG();
        }
    }

    public static void cluster(int population) {
        HillClimb hillClimb = new HillClimb();
        mdg = hillClimb.cluster(graph,new MDGImpl(), new ClusterImpl(), population);
    }

    public static void setGephi(){
        workspace = GephiInitializer.newWorkspace();
        graphModel = GephiInitializer.createDirectedGraph(graph, mdg);
    }

    public static void saveAs(FileProperties file) {
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
        SaveTask task = new SaveTask(pc.getCurrentProject(), new File(file.getLocation()));
        task.run();
        fileProperties = file;
        saved = true;
        History.addRecentFile(file);
    }

    public static void exportAsGraph(FileProperties file, boolean visibleOnly) {
        Export.exportAsGraph(file.getLocation(), file.getType(), visibleOnly);
    }

    public static void exportAsImage(FileProperties file) {
        Export.exportAsImage(file.getLocation());
    }

    public static void reset() {
        graph          = null;
        mdg            = null;
        saved          = false;
        fileProperties = null;
        graphModel     = null;
    }

    public static boolean isSaved() {
        return saved;
    }

    public static FileProperties getFileProperties() {
        return fileProperties;
    }

    public static Workspace getWorkspace() {
        return workspace;
    }

    public static GraphModel getGraphModel() {
        return graphModel;
    }

    public static MDG getMdg() {
        return mdg;
    }

    public static Graph getGraph() {
        return graph;
    }

    public static SheafFrame.MainPanel getMainPanel() {
        return mainPanel;
    }

    public static void setMainPanel(SheafFrame.MainPanel mainPanel) {
        Project.mainPanel = mainPanel;
    }

    @SuppressWarnings("all")
    private static void createMDG(){
        GraphInitializer initializer = new GraphInitializer();
        graph = initializer.createGraph.apply(graphModel.getDirectedGraph());
        mdg = new MDGImpl();
        //create clusters
        Map<String, Cluster> clusterMap = new HashMap<>();
        graphModel.getDirectedGraph().getNodes().forEach( node -> {
            String clusterId = (String) node.getAttribute(GephiInitializer.CLUSTER);
            if(!clusterMap.containsKey(clusterId)){
                Cluster cluster = new ClusterImpl();
                cluster.setId(clusterId);
                mdg.addCluster(cluster);
                clusterMap.put(clusterId, cluster);
            }
        });

        graph.getNodes().forEach(
                node -> graphModel.getDirectedGraph().getNodes().forEach(gNode -> {
            if((int) Float.parseFloat((String) gNode.getId()) == (node.getId())){
                mdg.addNodeToTargetCluster(clusterMap.get((String) gNode.getAttribute(GephiInitializer.CLUSTER)), node);
            }
        }));
    }

}
