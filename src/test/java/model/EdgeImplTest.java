package model;

import model.impl.EdgeImpl;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;

/**
 * @author Georgia Grigoriadou
 */

public class EdgeImplTest {

    @Test
    public void testGetSourceID() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        final Field field   = edge.getClass().getDeclaredField("sourceID");
        field.setAccessible(true);
        int sourceID = 0;
        field.set(edge, sourceID);

        int result = edge.getSourceID();

        Assert.assertEquals(sourceID, result);
    }

    @Test
    public void testSetSourceID() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        int sourceID        = 0;
        edge.setSourceID(sourceID);

        final Field field = edge.getClass().getDeclaredField("sourceID");
        field.setAccessible(true);

        Assert.assertEquals(field.get(edge), sourceID);
    }

    @Test
    public void testGetTargetID() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        final Field field   = edge.getClass().getDeclaredField("targetID");
        field.setAccessible(true);
        int targetID = 0;
        field.set(edge, targetID);

        int result = edge.getTargetID();

        Assert.assertEquals(targetID, result);
    }

    @Test
    public void testSetTargetID() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        int targetID        = 0;
        edge.setTargetID(targetID);

        final Field field = edge.getClass().getDeclaredField("targetID");
        field.setAccessible(true);

        Assert.assertEquals(field.get(edge), targetID);
    }

    @Test
    public void testGetType() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        final Field field   = edge.getClass().getDeclaredField("type");
        field.setAccessible(true);
        String type = "Directed";
        field.set(edge, type);

        String result = edge.getType();

        Assert.assertEquals(type, result);
    }

    @Test
    public void testSetType() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        String type = "Directed";
        edge.setType(type);

        final Field field = edge.getClass().getDeclaredField("type");
        field.setAccessible(true);

        Assert.assertEquals(field.get(edge), type);
    }

    @Test
    public void testGetWeight() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        final Field field   = edge.getClass().getDeclaredField("weight");
        field.setAccessible(true);
        float weight = (float) 1.0;
        field.set(edge, weight);

        float result = edge.getWeight();

        Assert.assertEquals(weight, result, 0.0);
    }

    @Test
    public void testSetWeight() throws Exception {
        final EdgeImpl edge = new EdgeImpl();
        float weight        = (float) 1.0;
        edge.setWeight(weight);

        final Field field = edge.getClass().getDeclaredField("weight");
        field.setAccessible(true);

        Assert.assertEquals(field.get(edge), weight);
    }

}
