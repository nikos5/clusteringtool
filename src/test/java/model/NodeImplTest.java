package model;

import model.api.Node;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * @author Georgia Grigoriadou
 */

public class NodeImplTest {

    @Test
    public void testGetId() throws Exception {
        final NodeImpl node = new NodeImpl();
        final Field field   = node.getClass().getDeclaredField("id");
        field.setAccessible(true);
        int id = 0;
        field.set(node, id);

        int result = node.getId();

        Assert.assertEquals(id, result);
    }

    @Test
    public void testGetName() throws Exception {
        final NodeImpl node = new NodeImpl();
        final Field field   = node.getClass().getDeclaredField("name");
        field.setAccessible(true);
        String name = "test_node";
        field.set(node, name);

        String result = node.getName();

        Assert.assertEquals(name, result);
    }



    @Test
    public void testGetNeighbours() throws Exception{
        final NodeImpl node = new NodeImpl();
        final Field field   = node.getClass().getDeclaredField("neighbours");
        field.setAccessible(true);
        HashMap<Node, Float> neighbours = new HashMap<>();
        Node neighbour = new NodeImpl();
        neighbours.put(neighbour, (float) 1.0);
        field.set(node, neighbours);

        HashMap result = node.getNeighbours();

        Assert.assertEquals(neighbours, result);
    }

    @Test
    public void testAddNeighbour() throws Exception{
        final NodeImpl node = new NodeImpl();
        NodeImpl neighbour  = new NodeImpl();
        final Field field   = node.getClass().getDeclaredField("neighbours");
        field.setAccessible(true);

        node.addNeighbour(neighbour, (float) 1.0);

        HashMap result = node.getNeighbours();

        Assert.assertTrue(result.containsKey(neighbour));
        Assert.assertTrue(result.containsValue((float) 1.0));
    }
}
