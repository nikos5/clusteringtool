package controller;

import model.api.Edge;
import model.api.Graph;
import model.api.Node;
import model.impl.EdgeImpl;
import model.impl.GraphImpl;
import model.impl.NodeImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Georgia Grigoriadou
 */

@SuppressWarnings("unchecked")
public class GraphInitializerTest {

    private Graph graph  = new GraphImpl();

    @SuppressWarnings("Duplicates")
    @Before
    public void setup() {
        Node nodeA  = new NodeImpl(1, "A");
        Node nodeB  = new NodeImpl(2, "B");
        Node nodeC  = new NodeImpl(3, "C");
        Node nodeD  = new NodeImpl(4, "D");
        Node nodeE  = new NodeImpl(5, "E");
        Node nodeF  = new NodeImpl(6, "F");
        Edge edge1  = new EdgeImpl(1, 2, "Directed", 7);
        Edge edge2  = new EdgeImpl(1, 3, "Directed", 1);
        Edge edge3  = new EdgeImpl(2, 3, "Directed", 5);
        Edge edge4  = new EdgeImpl(5, 2, "Directed", 2);
        Edge edge5  = new EdgeImpl(4, 2, "Directed", 4);
        Edge edge6  = new EdgeImpl(2, 6, "Directed", 1);
        Edge edge7  = new EdgeImpl(3, 6, "Directed", 7);
        Edge edge8  = new EdgeImpl(3, 5, "Directed", 2);
        Edge edge9  = new EdgeImpl(4, 5, "Directed", 5);
        Edge edge10 = new EdgeImpl(5, 6, "Directed", 1);

        nodeA.addNeighbour(nodeB, (float) 7.0);
        nodeA.addNeighbour(nodeC, (float) 1.0);
        nodeB.addNeighbour(nodeA, (float) 7.0);
        nodeB.addNeighbour(nodeC, (float) 5.0);
        nodeB.addNeighbour(nodeD, (float) 4.0);
        nodeB.addNeighbour(nodeE, (float) 2.0);
        nodeB.addNeighbour(nodeF, (float) 1.0);
        nodeC.addNeighbour(nodeA, (float) 1.0);
        nodeC.addNeighbour(nodeB, (float) 5.0);
        nodeC.addNeighbour(nodeE, (float) 2.0);
        nodeC.addNeighbour(nodeF, (float) 7.0);
        nodeD.addNeighbour(nodeB, (float) 4.0);
        nodeD.addNeighbour(nodeE, (float) 5.0);
        nodeE.addNeighbour(nodeB, (float) 2.0);
        nodeE.addNeighbour(nodeC, (float) 2.0);
        nodeE.addNeighbour(nodeD, (float) 4.0);
        nodeE.addNeighbour(nodeF, (float) 1.0);
        nodeF.addNeighbour(nodeB, (float) 1.0);
        nodeF.addNeighbour(nodeC, (float) 7.0);
        nodeF.addNeighbour(nodeE, (float) 1.0);

        Set<Node> nodes = new HashSet<>();
        Set<Edge> edges = new HashSet<>();

        nodes.add(nodeA);
        nodes.add(nodeB);
        nodes.add(nodeC);
        nodes.add(nodeD);
        nodes.add(nodeE);
        nodes.add(nodeF);
        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);
        edges.add(edge6);
        edges.add(edge7);
        edges.add(edge8);
        edges.add(edge9);
        edges.add(edge10);

        graph.setNodes(nodes);
        graph.setEdges(edges);
    }

    @Test
    public void testInitialize() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();

        final String nodesPath = "src/main/resources/tests/test4/nodes.csv";
        final String edgesPath = "src/main/resources/tests/test4/edges.csv";

        Graph newGraph = graphInitializer.initialize.apply(nodesPath, edgesPath, true);


        Assert.assertEquals(graph.getNodes().size(), newGraph.getNodes().size());
        Assert.assertEquals(graph.getEdges().size(), newGraph.getEdges().size());


        for(Node newNode : newGraph.getNodes()){
            boolean found = false;
            for(Node node : graph.getNodes()){
                if(node.getId() == newNode.getId() && node.getName().equals(newNode.getName())){
                    found = true;
                    Assert.assertEquals(node.getNeighbours().size(), newNode.getNeighbours().size());

                    for(Map.Entry<Node, Float> neighbour : node.getNeighbours().entrySet()){
                        boolean neighbourFound = false;
                        for(Map.Entry<Node, Float> newNeighbour : newNode.getNeighbours().entrySet()){
                            if (neighbour.getKey().getId() == newNeighbour.getKey().getId()){
                                neighbourFound = true;
                                break;
                            }
                        }
                        Assert.assertTrue(neighbourFound);
                    }
                    break;
                }
            }
            Assert.assertTrue(found);
        }

        for(Edge newEdge : newGraph.getEdges()){
            boolean found     = false;
            boolean identical = false;
            for(Edge edge : graph.getEdges()){
                if(edge.getSourceID() == newEdge.getSourceID() && edge.getTargetID() == newEdge.getTargetID()){
                    found = true;
                    if(edge.getType().equals(newEdge.getType()) && edge.getWeight() == newEdge.getWeight()){
                        identical = true;
                    }
                    break;
                }
            }
            Assert.assertTrue(found);
            Assert.assertTrue(identical);
        }
    }



    @Test
    public void testReadCSVFile() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();
        final String path                       = "src/main/resources/tests/test4/nodes.csv";

        Function<String[], Object> function = (line)-> new Object();

        final Field readCVSFileField = graphInitializer.getClass().getDeclaredField("readCSVFile");
        readCVSFileField.setAccessible(true);
        BiFunction<String, Function<String[], Object>, Set<Object>> readCVSFile
                = (BiFunction<String, Function<String[], Object>, Set<Object>>) readCVSFileField.get(graphInitializer);

        Set<Object> objects = readCVSFile.apply(path, function);
        Assert.assertEquals(objects.size(), 7);
    }


    @Test
    public void testCreateNode() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();
        final String[] line                     = new String[2];
        line[0]                                 = String.valueOf(1);
        line[1]                                 = "testNode";

        final Field createNodeField = graphInitializer.getClass().getDeclaredField("createNode");
        createNodeField.setAccessible(true);
        Function<String[], Object> createNode = (Function<String[], Object>) createNodeField.get(graphInitializer);

        Node node = (Node) createNode.apply(line);

        final Field nmField = graphInitializer.getClass().getDeclaredField("nodesMap");
        nmField.setAccessible(true);
        HashMap<Integer, Node> nodesMap = (HashMap<Integer, Node>) nmField.get(graphInitializer);

        Assert.assertEquals(node.getId(), 1);
        Assert.assertEquals(node.getName(), "testNode");
        Assert.assertTrue(nodesMap.containsKey(1));
        Assert.assertEquals(nodesMap.get(1).getId(), 1);
        Assert.assertEquals(nodesMap.get(1).getName(), "testNode");
        Assert.assertEquals(node, nodesMap.get(1));
    }

    @Test
    public void testCreateNodeWrongTypeFail() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();
        final String[] line                     = new String[2];
        line[0]                                 = "notInteger";
        line[1]                                 = "testNode";

        final Field createNodeField = graphInitializer.getClass().getDeclaredField("createNode");
        createNodeField.setAccessible(true);
        Function<String[], Object> createNode = (Function<String[], Object>) createNodeField.get(graphInitializer);

        Node node = (Node) createNode.apply(line);

        final Field nmField = graphInitializer.getClass().getDeclaredField("nodesMap");
        nmField.setAccessible(true);

        Assert.assertNull(node);
    }

    @Test
    public void testCreateNodeNullPointerFail() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();

        final Field createNodeField = graphInitializer.getClass().getDeclaredField("createNode");
        createNodeField.setAccessible(true);
        Function<String[], Object> createNode = (Function<String[], Object>) createNodeField.get(graphInitializer);

        Node node = (Node) createNode.apply(null);

        final Field nmField = graphInitializer.getClass().getDeclaredField("nodesMap");
        nmField.setAccessible(true);

        Assert.assertNull(node);
    }

    @Test
    public void testCreateEdges() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();
        final String[] line                     = new String[4];
        line[0]                                 = "1";
        line[1]                                 = "2";
        line[2]                                 = "Directed";
        line[3]                                 = "1";


        final Field createEdgeField = graphInitializer.getClass().getDeclaredField("createEdge");
        createEdgeField.setAccessible(true);
        Function<String[], Object> createEdge = (Function<String[], Object>) createEdgeField.get(graphInitializer);

        Edge edge = (Edge) createEdge.apply(line);

        Assert.assertEquals(edge.getSourceID(), 1);
        Assert.assertEquals(edge.getTargetID(), 2);
        Assert.assertEquals(edge.getType(), "Directed");
        Assert.assertEquals(edge.getWeight(), (float) 1.0, 0.0);
    }

    @Test
    public void testCreateEdgeWrongTypeFail() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();
        final String[] line                     = new String[4];
        line[0]                                 = "NotInteger";
        line[1]                                 = "2";
        line[2]                                 = "Directed";
        line[3]                                 = "1";

        final Field createEdgeField = graphInitializer.getClass().getDeclaredField("createEdge");
        createEdgeField.setAccessible(true);
        Function<String[], Object> createEdge = (Function<String[], Object>) createEdgeField.get(graphInitializer);

        Edge edge = (Edge) createEdge.apply(line);

        Assert.assertNull(edge);
    }

    @Test
    public void testCreateEdgeNullPointerFail() throws Exception{
        final GraphInitializer graphInitializer = new GraphInitializer();

        final Field createEdgeField = graphInitializer.getClass().getDeclaredField("createEdge");
        createEdgeField.setAccessible(true);
        Function<String[], Object> createEdge = (Function<String[], Object>) createEdgeField.get(graphInitializer);

        Edge edge = (Edge) createEdge.apply(null);

        Assert.assertNull(edge);
    }

    @Test
    public void testAddNeightbours() throws Exception{
        final GraphInitializer graphInitializer   = new GraphInitializer();
        ConcurrentHashMap<Integer, Node> nodesMap = new ConcurrentHashMap<>();
        Graph initialGraph                        = graph;
        for (Node node : initialGraph.getNodes()){
            nodesMap.put(node.getId(), node);
            node.getNeighbours().clear();
        }

        final Field nmField = graphInitializer.getClass().getDeclaredField("nodesMap");
        nmField.setAccessible(true);
        nmField.set(graphInitializer, nodesMap);

        final Field addNeighboursField = graphInitializer.getClass().getDeclaredField("addNeighbours");
        addNeighboursField.setAccessible(true);
        BiFunction<Graph,Boolean, Graph> addNeighbours = ( BiFunction<Graph,Boolean, Graph>) addNeighboursField.get(graphInitializer);

        Graph finalGraph = addNeighbours.apply(initialGraph, true);

        Iterator<Node> iterator      = graph.getNodes().iterator();
        Iterator<Node> finalIterator = finalGraph.getNodes().iterator();

        while (iterator.hasNext()) {
            Node node           = iterator.next();
            Node finalGraphNode = finalIterator.next();

            Assert.assertEquals(node.getNeighbours().size(), finalGraphNode.getNeighbours().size());

            for(Map.Entry<Node, Float> neighbour : node.getNeighbours().entrySet()){
                boolean neighbourFound = false;
                for(Map.Entry<Node, Float> finalNodeNeighbour : finalGraphNode.getNeighbours().entrySet()){
                    if (neighbour.getKey() == finalNodeNeighbour.getKey() && neighbour.getValue().equals(finalNodeNeighbour.getValue())){
                        neighbourFound = true;
                        break;
                    }
                }
                Assert.assertTrue(neighbourFound);
            }
        }
    }

}
